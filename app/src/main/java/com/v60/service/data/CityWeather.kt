package com.v60.service.data

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CityWeather(@JsonProperty("type") val type: TypeOfWeather,
                       @JsonProperty("temperatureInCelsius") val temperatureInCelsius: Int,
                       @JsonProperty("city") val city: String) :
    Parcelable {
}