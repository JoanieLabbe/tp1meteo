package com.v60.service.data

enum class TypeOfWeather() {
    CLOUDY(),
    PARTLY_SUNNY(),
    RAIN(),
    SNOW(),
    SUNNY(),
    ERROR(),
    NONE(),
    PROGRESS()
}