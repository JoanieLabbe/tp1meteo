package com.v60.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.v60.service.data.CityWeather
import org.androidannotations.annotations.EBean
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.io.IOException

private const val BASE_URL = "https://tp1-mobile2.herokuapp.com/"

@EBean(scope = EBean.Scope.Singleton)
class WeatherService {
    interface Service {
        @GET("meteo/{city}")
        fun findWeatherInCity(@Path("city") city: String) : Call<CityWeather>
    }

    private val service: Service
    init {
        val jackson = ObjectMapper().registerKotlinModule()

        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create(jackson))
            .build()

        service = retrofit.create(Service::class.java)
    }

    fun findWeatherInCity(city: String) : CityWeather {
        val call: Call<CityWeather> = service.findWeatherInCity(city)

        val response: Response<CityWeather> = call.execute()

        if(response.isSuccessful) {
            var result = response.body()
            if(result != null) {
                return result
            }
        }
        throw IOException(response.message().toString())
    }
}