package com.v60.tp1client

import com.v60.service.data.CityWeather
import com.v60.service.data.TypeOfWeather

fun CityWeather.getImageToDisplay(): Int {
    return when (this.type) {
        TypeOfWeather.CLOUDY -> R.drawable.ic_cloudy
        TypeOfWeather.PARTLY_SUNNY -> R.drawable.ic_partly_sunny
        TypeOfWeather.RAIN -> R.drawable.ic_rain
        TypeOfWeather.SNOW -> R.drawable.ic_snow
        TypeOfWeather.SUNNY -> R.drawable.ic_sunny
        else -> R.drawable.ic_umbrella
    }
}