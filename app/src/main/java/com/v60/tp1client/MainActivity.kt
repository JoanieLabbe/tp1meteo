package com.v60.tp1client

import android.view.View.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.Group
import org.androidannotations.annotations.*
import com.v60.service.WeatherService
import com.v60.service.data.CityWeather
import com.v60.service.data.TypeOfWeather
import java.io.IOException

@EActivity(R.layout.activity_main)
class MainActivity : AppCompatActivity() {

    @ViewById(R.id.groupWeather)
    protected lateinit var groupWeather: Group
    @ViewById(R.id.groupError)
    protected lateinit var groupError: Group
    @ViewById(R.id.txtTemperatureValue)
    protected lateinit var txtTemperatureValue: TextView
    @ViewById(R.id.txtCityName)
    protected lateinit var txtCityName: TextView
    @ViewById(R.id.imgWeather)
    protected lateinit var imgWeather: ImageView
    @ViewById (R.id.progressBar)
    protected lateinit var progressBar: ProgressBar
    @ViewById (R.id.searchBar)
    protected lateinit var searchBar: SearchView

    @Bean
    protected lateinit var weatherService: WeatherService

    @InstanceState
    protected lateinit var cityWeather: CityWeather

    @AfterViews
    protected fun onViewsInjected() {
        if(!this::cityWeather.isInitialized) {
            cityWeather = CityWeather(TypeOfWeather.NONE, 0, "")
        }
        restoreProgress()
        setListenerOnSearchBar()
        updateDisplay()
    }

    @Click(R.id.btnRetry)
    protected fun onBtnRetryClicked(){
        if(cityWeather.type != TypeOfWeather.PROGRESS) {
            findWeatherInCity(cityWeather.city)
        }
    }

    @Background
    protected fun findWeatherInCity(city: String) {
        startProgress(city)
        cityWeather = try {
            weatherService.findWeatherInCity(city)
        } catch (e: IOException) {
            CityWeather(TypeOfWeather.ERROR, 0, city)
        }
        updateDisplay()
    }

    @UiThread
    protected fun displayProgressBar() {
        changeVisibility(GONE,GONE,VISIBLE)
    }

    @UiThread
    protected fun displayDefault() {
        changeVisibility(GONE,GONE,GONE)
    }

    @UiThread
    protected fun displayWeather(cityWeather: CityWeather) {
        txtTemperatureValue.text = cityWeather.temperatureInCelsius.toString() + getString(R.string.text_temperature)
        imgWeather.setImageResource(cityWeather.getImageToDisplay())
        txtCityName.text = cityWeather.city

        changeVisibility(VISIBLE, GONE, GONE)
    }

    @UiThread
    protected fun displayError() {
        changeVisibility(GONE, VISIBLE, GONE)
        searchBar.setQuery(getString(R.string.invalid_query), false)
    }

    private fun setListenerOnSearchBar() {
        searchBar.setOnQueryTextListener(object : SearchView.OnQueryTextListener{

            override fun onQueryTextSubmit(query: String): Boolean {
                if(cityWeather.type != TypeOfWeather.PROGRESS)
                {
                    findWeatherInCity(query)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // Rien à faire ici
                return false
            }

        })
    }

    private fun startProgress(city: String) {
        cityWeather = CityWeather(TypeOfWeather.PROGRESS, 0, city)
        updateDisplay()
    }

    private fun updateDisplay() {
        when (cityWeather.type) {
            TypeOfWeather.NONE -> displayDefault()
            TypeOfWeather.ERROR -> displayError()
            TypeOfWeather.PROGRESS -> displayProgressBar()
            else -> displayWeather(cityWeather)
        }
    }

    private fun changeVisibility(temperatureVisibility:Int, errorVisibility:Int, progressBarVisibility:Int){
        groupWeather.visibility = temperatureVisibility
        groupError.visibility = errorVisibility
        progressBar.visibility = progressBarVisibility
    }

    private fun restoreProgress() {
        if(cityWeather.type == TypeOfWeather.PROGRESS) {
            findWeatherInCity(cityWeather.city)
        }
    }

}